# vi: set sw=2 ts=2 ai et:

class yumrepos {

  case $::operatingsystem {

    'OracleLinux' : {

     case $::operatingsystemrelease {
        
        /^5/: {
          # ol5 repos
        }

        /^6/: {
          file { '/etc/yum.repos.d/public-yum-ol6.repo':
            source  => "puppet:///modules/${module_name}/public-yum-ol6.repo",
            ensure  => present,
          }
          exec { 'yumclean':
            command     => 'yum clean all',
            refreshonly => true,
          }
          
        }

      }

    }

    'CentOS' : {
      # centos repo
    }

  }

}
