# vi: set sw=2 ts=2 ai et:

# Class: ssh::service
class ssh::service {
  service { $::ssh::service_name:
    ensure     => $::ssh::service_ensure,
    enable     => $::ssh::service_enable,
    hasrestart => $::ssh::service_hasrestart,
    hasstatus  => $::ssh::service_hasstatus,
    require    => Package[$::ssh::pkg_list],
  }
}
