# vi: set sw=2 ts=2 ai et:

# Class: ssh
class ssh::params {
  $listenaddress      = [ '0.0.0.0', '[::]' ]
  $listenport         = '22'
  $forwardx           = 'yes'
  $addressfamily      = 'any'
  #
  $config_file        = '/etc/ssh/sshd_config'
  $config_template    = "${module_name}/sshd_config.erb"
  $config_user        = 'root'
  $config_mode        = '0600'
  $pkg_ensure         = present
  $service_ensure     = running
  $service_enable     = true
  $service_hasstatus  = true
  $service_hasrestart = true

  # Settings per OS
  case $::osfamily {
    'RedHat' : {
      $config_group       = 'root'
      $pkg_list           = 'openssh-server'
      $service_name       = 'sshd'
    }

    'Debian' : {
      $config_group       = 'root'
      $pkg_list           = 'ssh'
      $service_name       = 'ssh'
    }

    'FreeBSD' : {
      $config_group       = 'wheel'
      $pkg_list           = 'openssh-server'
      $service_name       = 'sshd'
    }

    default: {
      fail("Module '${module_name}': ${::osfamily} v ${::lsbmajdistrelease} not supported!")
    }
  }
}
