# vi: set sw=2 ts=2 ai et:

# Class: ssh::install
class ssh::install {
  if $::ssh::pkg_list != undef {
    package { $::ssh::pkg_list :
      ensure => $::ssh::pkg_ensure,
    }
  }
}
