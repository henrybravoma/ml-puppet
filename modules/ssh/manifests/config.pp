# vi: set sw=2 ts=2 ai et:

# Class: ssh::config
class ssh::config {
  file { $::ssh::config_file :
    ensure  => file,
    path    => $::ssh::config_file,
    owner   => $::ssh::config_user,
    group   => $::ssh::config_group,
    mode    => $::ssh::config_mode,
    content => template($::ssh::config_template),
    notify  => Service[$::ssh::service_name],
    require => Package[$::ssh::pkg_list],
  }
}
