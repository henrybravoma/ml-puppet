# vi: set sw=2 ts=2 ai et :

# Class: ssh
class ssh (
    $listenaddress      = $::ssh::params::listenaddress,
    $listenport         = $::ssh::params::listenport,
    $forwardx           = $::ssh::params::forwardx,
    $addressfamily      = $::ssh::params::addressfamily,
    #
    $config_file        = $::ssh::params::config_file,
    $config_source      = $::ssh::params::config_source,
    $config_template    = $::ssh::params::config_template,
    $config_user        = $::ssh::params::config_user,
    $config_group       = $::ssh::params::config_group,
    $config_mode        = $::ssh::params::config_mode,
    $pkg_list           = $::ssh::params::pkg_list,
    $pkg_ensure         = $::ssh::params::pkg_ensure,
    $service_name       = $::ssh::params::service_name,
    $service_ensure     = $::ssh::params::service_ensure,
    $service_enable     = $::ssh::params::service_enable,
    $service_hasstatus  = $::ssh::params::service_hasstatus,
    $service_hasrestart = $::ssh::params::service_hasrestart,
  ) inherits ssh::params {

  notify { "ModMsg_${module_name}" :
    message  => "Entered 'init.pp' of module '${module_name}'",
    withpath => false,
  }

  include ssh::install
  include ssh::config
  include ssh::service

  Class['ssh::install']
    -> Class['ssh::config']
    -> Class['ssh::service']
}
