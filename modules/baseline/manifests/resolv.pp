# vi: set sw=2 ts=2 ai et:

# Class: default::resolv
class baseline::resolv {
  # Get the parameters from Hiera
  $nameservers = hiera("resolv::nameservers", '')
  $domain      = hiera("resolv::domain",      '')
  $search      = hiera("resolv::search",      '')

  # Call the global / generic class
  class { '::resolv' :
    nameservers => $nameservers,
    domain      => $domain,
    search      => $search,
  }

}
