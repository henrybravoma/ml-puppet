# vi: set sw=2 ts=2 ai et:

# Class: default::ssh
class baseline::ssh {
  # Get the parameters from Hiera
  $listenaddress = hiera('ssh::listenaddress', '')
  $listenport    = hiera('ssh::listenport',    '')
  $forwardx      = hiera('ssh::forwardx',      '')
  $addressfamily = hiera('ssh::addressfamily', '')

  # Call the global / generic class
  class { '::ssh' :
    listenaddress => $listenaddress,
    listenport    => $listenport,
    forwardx      => $forwardx,
    addressfamily => $addressfamily,
  }
}
