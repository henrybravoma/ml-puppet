# vi: set sw=2 ts=2 ai et:

# Class: default::ntp
class baseline::ntp {
  $servers = hiera('ntp::servers')
  $master  = hiera('ntp::master')
  $peer    = hiera('ntp::peer', '')
  $allow   = hiera('ntp::allow', '')

  class { '::ntp' :
    servers => $servers,
    master  => $master,
    peer    => $peer,
    allow   => $allow,
  }
}
