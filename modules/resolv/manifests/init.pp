# vi: set sw=2 ts=2 ai et:

# Class: resolv
class resolv (
    $nameservers     = $::resolv::params::nameservers,
    $domain          = $::resolv::params::domain,
    $search          = $::resolv::params::search,
    #
    $config_file     = $::resolv::params::config_file,
    $config_template = $::resolv::params::config_template,
    $config_user     = $::resolv::params::config_user,
    $config_group    = $::resolv::params::config_group,
    $config_mode     = $::resolv::params::config_mode,
  ) inherits resolv::params {

  include resolv::config
}
