# vi: set sw=2 ts=2 ai et:

# Class: resolv
class resolv::params {
  $nameservers     = ''
  $domain          = ''
  $search          = ''
  #
  $config_file     = '/etc/resolv.conf'
  $config_template = "${module_name}/resolv.conf.erb"
  $config_user     = 'root'
  $config_mode     = '0644'

  case $::osfamily {
    'FreeBSD' : {
      $config_group = 'wheel'
    }

    default   : {
      $config_group = 'root'
    }
  }
}
