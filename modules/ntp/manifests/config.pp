# Class: ntp
class ntp::config {
  file { $::ntp::config_file :
    ensure  => file,
    path    => $::ntp::config_file,
    owner   => $::ntp::config_user,
    group   => $::ntp::config_group,
    mode    => $::ntp::config_mode,
    content => template($::ntp::config_template),
    notify  => Service[$::ntp::service_name],
    require => Package[$::ntp::pkg_list],
  }
}
