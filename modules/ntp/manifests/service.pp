# Class: ntp
class ntp::service {
  service { $::ntp::service_name:
    ensure     => $::ntp::service_ensure,
    enable     => $::ntp::service_enable,
    hasrestart => $::ntp::service_hasrestart,
    hasstatus  => $::ntp::service_hasstatus,
    require    => Package[$::ntp::pkg_list],
  }
}
