#!/bin/bash
# Name        : ml-puppet
# Author      : Henry Bravo
# Date        : Feb 2015
# ======================================
NAME="ml-puppet run"
VERSION=0.1
CUR_PWD=$(pwd)
PUPPETDIR="/opt/ml-puppet"
PUPPET_MODULES="$PUPPETDIR/modules"
HIERA_CONFIG="$PUPPETDIR/hiera/hiera.yaml"
PUPPET_MANIFESTS="$PUPPETDIR/manifests/site.pp"
PUPPETRUN="$PUPPETDIR/ml-puppet.sh"
PUPPETLOGDIR="/var/log/ml-puppet"
PUPPETLOGFILE="$PUPPETLOGDIR/ml-puppet.log"
RED_TEXT="\e[31m"
GREEN_TEXT="\e[92m"
RESET_TEXT="\e[0m"

# Check if user is root
if [ "$(id -u)" != "0" ]; then
   echo -e "$RED_TEXT"
   echo "This script must be run as root" 1>&2
   echo -e "$RESET_TEXT"
   exit 1
fi

# log actions
echo "==== $(date "+%a %b %d %H:%M:%S") -=- starting puppet run ===" >>"$PUPPETLOGFILE"
exec >& >(tee -a "$PUPPETLOGFILE")

cd $PUPPETDIR

# respond to cmd line arguments
while [ -n "$1" ]; do
    case $1 in
        -v|--version)
            echo -e "$NAME version: $VERSION"
            echo -e "on opensource puppet version: $(puppet --version)"
            echo -e "----------------------------------------------------------"
            echo -e ""
            exit 0 ;;

        -h|--help)
            echo -e "$NAME version: $VERSION"
            echo -e "on opensource puppet version: $(puppet --version)"
            echo -e "----------------------------------------------------------"
            echo -e "Usage: ./ml-puppet.sh [OPTION]"
            echo -e ""
            echo -e "no options     -  default usage"
            echo -e "-v, --version  -  show current version"
            echo -e "-h, --help     -  print this help"
            echo -e "--noop         -  to do a test run, without applying the configuration"
            echo -e ""
            exit 0 ;;

        --noop)
            echo -e "$RED_TEXT"
            echo "Applying noop puppet configuration to host: $(facter fqdn)" ; echo -e "$RESET_TEXT"
            puppet apply --modulepath "$PUPPET_MODULES" --hiera_config="$HIERA_CONFIG" "$PUPPET_MANIFESTS" "$1"
            exit 0 ;;

        --)
            shift; break ;; # end of options
        -*)
            echo "error: no such option $1. -h for help"; exit 1 ;;
         *) break; exit 0 ;;

    esac
done

# Run puppet config
###################
if [ -e $1 ] ; then
    echo -e "$GREEN_TEXT"
    echo "Applying puppet configuration to host: $(facter fqdn)" ; echo -e "$RESET_TEXT"
    puppet apply --modulepath "$PUPPET_MODULES" --hiera_config="$HIERA_CONFIG" "$PUPPET_MANIFESTS"
fi
