# vi: set sw=2 ts=2 ai et:

notify { 'SiteMessage' :
  message  => "============ running site.pp ============",
  withpath => false,
}

# Set the defaults for files
File {
  owner => 'root',
  group => $::group0,
  mode  => '0644',
}

# Settings for execs
Exec {
  path => [ '/bin/',
            '/sbin/',
            '/usr/bin/',
            '/usr/sbin/',
            '/usr/local/bin/',
            '/usr/local/sbin/',
  ],
}

# Don't distribute version control metadata
File {
  ignore => [ '.svn',
              '.git',
              '.bzr',
              '.hg',
              'CVS',
              '.project',
  ],
}

# Include the defaults class
hiera_include('classes')

# Install the default users
class {'accounts': }

# Module & Package Management
class { 'sudo' :
  purge               => false,
  config_file_replace => false,
}
class { '::yumrepos' : }

# Install default packages
package { [ "openssh-clients",
            "telnet",
            "screen",
            "mlocate",
            "zip",
            "unzip",
          ]:
    ensure => "installed",
}

# Create the cron job for the puppet runs
cron { 'ml-puppet' :
  command => "bash -c /opt/ml-puppet/ml-puppet.sh",
  user    => root,
  hour    => '*',
  minute  => '*/30',
}

# Logrotate the puppet logfiles
logrotate::rule { 'ml-puppet':
  path          => '/var/log/ml-puppet/*.log',
  compress      => true,
  delaycompress => true,
  missingok     => true,
  rotate        => 31,
  rotate_every  => week,
  dateext       => true,
  ifempty       => false,
}

# Node specific configuration
case $::fqdn {
  # for local testing purposes
  /^localhost/: {
    notify { 'SiteFqdnMessage' :
      message  => "=============== running site.pp `case $::fqdn -> localhost test` ===============",
      withpath => false,
    }
  }
}

