#!/bin/bash
# Name        : ml-puppet
# Author      : Henry Bravo
# Date        : Feb 2015
# ======================================
NAME="ml-puppet install script"
VERSION=0.1
CUR_PWD=$(pwd)
PUPPETDIR="/opt/ml-puppet"
PUPPET_MODULES="$PUPPETDIR/modules"
HIERA_CONFIG="$PUPPETDIR/hiera/hiera.yaml"
PUPPET_MANIFESTS="$PUPPETDIR/manifests/site.pp"
PUPPETRUN="$PUPPETDIR/ml-puppet.sh"
PUPPETLOGDIR="/var/log/ml-puppet"
PUPPETLOGFILE="$PUPPETLOGDIR/ml-puppet.log"
RED_TEXT='\e[31m'
GREEN_TEXT='\e[92m'
RESET_TEXT='\e[0m'

# banner
echo -e "$GREEN_TEXT"
echo -e "   __  _____      ___  __  _____  ___  __________"
echo -e "  /  |/  / / ____/ _ \/ / / / _ \/ _ \/ __/_  __/"
echo -e " / /|_/ / /_/___/ ___/ /_/ / ___/ ___/ _/  / /"
echo -e "/_/  /_/____/  /_/   \____/_/  /_/  /___/ /_/"
echo -e ""
echo -e " $NAME v.$VERSION"
echo -e "================================$RESET_TEXT"
echo ""

# Check if user is root
if [ "$(id -u)" != "0" ]; then
   echo -e "$RED_TEXT This script must be run as root" 1>&2 && echo -e "$RESET_TEXT"
   exit 1
fi

OPT_DIR="/opt"
if [ ! -d "$OPT_DIR" ] ; then
    echo -e "$RED_TEXT"
    echo -e "there is no /opt on your system. ml-puppet needs this directory, please create it and execute the installer again" ; echo -e "$RESET_TEXT"
    exit 1
fi

if [ -d "$PUPPETDIR" ] ; then
    echo -e "$RED_TEXT Dir: $PUPPETDIR exists, this is not a new install" && echo -e "$RESET_TEXT"
fi

if [ -d "$PUPPETLOGDIR" ] ; then
    echo -e "$RED_TEXT Dir: $PUPPETLOGDIR exists, this is not a new install" && echo -e "$RESET_TEXT"
fi

# create and set the $PUPPETLOGDIR
if [ ! -d "$PUPPETLOGDIR" ] ; then
    echo -e "$GREEN_TEXT"
    echo -e "create and set the $PUPPETLOGDIR" ; echo -e "$RESET_TEXT"
    mkdir "$PUPPETLOGDIR"
    chmod 0755 "$PUPPETLOGDIR"
    touch "$PUPPETLOGFILE" && chmod 0666 "$PUPPETLOGFILE"
fi

# Log this installer
exec >& >(tee -a "$PUPPETLOGFILE")

echo -e "$GREEN_TEXT"
echo -e "$(date "+%a %b %d %H:%M:%S") -- Preparing $(hostname) with $NAME ver. $VERSION" ; echo -e "$RESET_TEXT"

# Add the puppet repo for EL 5,6,7s
if [ ! -f "/etc/yum.repos.d/puppetlabs.repo" ] ; then
    echo -e "$GREEN_TEXT"
    echo -e "Adding the Puppet repo" ; echo -e "$RESET_TEXT"
    RELEASE=$(grep -o '[0-9]\+' /etc/*-release | head -1 | grep -o '[0-9]\+')
    if [[ $RELEASE == 21 || $RELEASE == 22 ]] ; then # for fedora
        rpm -ivh http://yum.puppetlabs.com/puppetlabs-release-el-7.noarch.rpm
    else
        rpm -ivh http://yum.puppetlabs.com/puppetlabs-release-el-$RELEASE.noarch.rpm
    fi
fi

# puppet needs this dir
FACTSDIR="/etc/facter"
if [ -f "$FACTSDIR" ] ; then
    ln -s $PUPPETDIR/facter/facts.d /etc/facter/facts.d
fi

# Check if pe-puppet agent is installed
if [ ! -e $(rpm -qa pe-puppet) ]; then
    echo -e "$RED_TEXT"
    echo -e "Puppet Enterprise is installed."$RESET_TEXT""$GREEN_TEXT" Uninstalling it now..." ; echo -e "$RESET_TEXT"
    yum remove -y pe-puppet pe-puppet-enterprise-release
    # remove the install dir /opt/puppet
fi

# Check if puppet agent is installed
if [ -e $(rpm -qa puppet) ]; then
    echo -e "$RED_TEXT"
    echo -e "Puppet is not installed. Installing it now..." ; echo -e "$RESET_TEXT"
    yum install -y puppet
fi

# Check if puppet-common is installed
if [ -e $(rpm -qa mcollective-puppet-common) ]; then
    echo -e "$RED_TEXT"
    echo -e "Puppet-common is not installed. Installing it now..." ; echo -e "$RESET_TEXT"
    yum install -y mcollective-puppet-common
fi

# install ruby & rubygems
if [ -e $(rpm -qa rubygems) ]; then
    echo -e "$RED_TEXT"
    echo -e "rubygems not installed. Installing it now..." ; echo -e "$RESET_TEXT"
    yum install -y rubygems
fi

# Check if we have the deep_merge gem installed. (required by hiera)
gem spec deep_merge > /dev/null 2>&1
if [ "$?" != "0" ]; then
    echo -e "$RED_TEXT"
    echo -e "Deep_merge is not installed. Installing it now..." ; echo -e "$RESET_TEXT"
    gem install deep_merge
fi

# Check if git is installed
if [ -e $(rpm -qa git) ]; then
    echo -e "$RED_TEXT"
    echo -e "Git is not installed. Installing it now..." ; echo -e "$RESET_TEXT"
    yum install -y git
fi

# Clone the git repo
echo -e "$GREEN_TEXT"
echo -e "clone git repo" ; echo -e "$RESET_TEXT"
if [ -d "$PUPPETDIR" ] ; then
    echo -e "$RED_TEXT"
    echo "Repo dir: $PUPPETDIR exists, please remove it first and then execute installer again" ; echo -e "$RESET_TEXT"
    exit 1
fi
#
cd /opt
git clone https://github.com/henrybravo/ml-puppet.git

# set perms & ownership to required
while [[ $(ps -ef | grep -v grep | grep git) != "" ]] ; do
    sleep 3
done
if [ -d "$PUPPETDIR" ] ; then
    chown -R root:root "$PUPPETDIR"
    find "$PUPPETDIR" -type f -exec chmod 0660 '{}' \;
    find "$PUPPETDIR" -type d -exec chmod 0750 '{}' \;
    cd "$PUPPETDIR"
    chmod 0777 *.sh
fi

# Done
echo -e "$GREEN_TEXT"
echo -e "$NAME has completed" ; echo -e "$RESET_TEXT"
