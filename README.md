# ml-puppet

Name        : masterless open-source puppet

Author      : Henry Bravo

Date        : Dec 2014

Platform    : Oracle Linux / Enterprise Linux - tested on CentOS6 & OL6

This is a masterless open-source puppet example setup, making use of hiera data. The puppet run is executed by the script `ml-puppet.sh` which is running by schedule which is configured in puppet. In a real life situation, the puppet code should be in a git repo, that would get cloned / pulled before every puppet run. A few modules are used from the forge - thanks to all of you contributing to the forge!

To install ml-puppet:

`bash <(curl -s https://raw.githubusercontent.com/henrybravo/ml-puppet/master/install.sh)`

Or copy and execute the `install.sh` manually. This will install ml-puppet in the /opt/ml-puppet directory.

The puppet run can be executed manually:

    # bash -c "/opt/ml-puppet/ml-puppet.sh"

The above command will:

* apply the manifests/site.pp manifest
* use the hiera config that can be found in hiera/hiera.yaml 
* search for modules in the modules directory.

You can append -h, -v or --noop :

For example:

    # ./ml-puppet --noop

Will simulate a puppet run.

### The bash scripts:

**ml-puppet.sh:**    
The puppet run, scheduled in cron

**install.sh:**    
Initial installer to prepare the systems for masterless open-source puppet. After installing it won't be used anymore, but it can be used to e.g. reset / restore and installation by moving it to /tmp and execute it from there, after deleting the ml-puppet tree.

### The puppet directories:

**ml-puppet:**    
The puppet installation and configuration

**hiera:**    
where all hiera data is stored

**manifests:**    
Where the manifests are stored. Will use the site.pp by default which also contains node definitions using $env and / or the $::fqdn global

**modules:**    
Where all modules are stored
